# The Yellow Taxi Analysis With PySpark

As role of Solution Architect, the end-to-end solution to demonstrate how to analyze the several dozens of millions of rows, several GB, of data per month from NYC TLC Trip Record Data (https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page), should meet the requirements as follows:

Notes: For easy to use, all the filename and db uri used in the notebooks here are for localhost. 

1. ETL stage ([ETL_local.ipynb](https://github.com/crossz/NYC_TAXI_YELLOW_TRENDS_ANALYSIS_SPARK2020/blob/master/ETL_local.ipynb))

   Using Spark to read and pre-preprocess the huge raw data of csv files:

   1) Retrieve data from raw format, 
   2) Perform data analysis (feature engineering) for data ETL

    As sated in the notebook, the python file used to run on EMR is 'ETL_mongo_connector_with_pyspark.py', while this 'ETL_local.ipynb' is good for local development and test.
    
2. Data Mining stage ([SIMULATION.ipynb](https://github.com/crossz/NYC_TAXI_YELLOW_TRENDS_ANALYSIS_SPARK2020/blob/master/SIMULATION.ipynb))

   Save the data into a database for further post-analysis and web portal design, and most visual illustrations are demonstrated during the simulation of taxi pickups on 10-min based data set and the web portal.

   GeoPandas is also demonstrated here in another section. Codes here are much simpler, and colormap is out of box.

   1) Illustrate request and demands trends, 
   2) Provide graphic illustration for operators to understand the changes of demands gap in different area (location ID), and simulate a real-time display of GUI on 10-min based data set for operators, 
   3) Let drivers to see the demands level in 10 min drive distance. If possible.

3. Web Portal Development (http://vue-taxi-2020.s3-us-west-2.amazonaws.com/index.html with any user name and password, and here is http so far)

   Use MongoDB or services from AWS, such as DocumentDB, OLAP services (RedShift, Apache Kylin on EMR), as data source to Setup a portal for different users to login to watch different GUI.

4. Modeling: python files with prefix of 'rf_' are for Random Forest modeling. 

5. Benchmark demos for AWS VS Local PC, there are basically 3 Notebook files.

## 1. ETL stage

For a csv file containing 10 millions of data rows, Data Frame technique is essential and provided in many ways, such as Pandas, Dash, Spark (v1 and v2). All of these scientific tools can achieve this purpose, but each has own disaster shortcoming and difficulties for post usage (the dataframe objects have huge differences for the methods from their own and most of them are not compatible), especially when the data going GB big.

To be simple here, for big amount of data reading:
- Pandas is slow compared to Dash, as the latter splits the files into parts and keep loading/saving into memory or IO underneath to ensure the capability to cope with big data, while Pandas load all of these into memory. 
- Spark is working on Hadoop Yarn, and takes advantage of JVM memory and multiple threading capabilities to improve the performance. RDD was popular during the time of spark V1, whereas nowadays, Spark DataFrame from V2 dominates all Spark world. Spark dataframe utilizes RDD underneath and more SQL like ways are all supported well from V2, such as select(), agg(), groupby(), when() etc.

Therefore, after investigating all tools above with the 10 millions of taxi data, Spark V2 is the final decision, Pandas is slightly used for local development and modeling.

## 2. Data Mining stage

Even if Spark and other EMR services are great, but they are just for calculation and stateless, the processed data have to be saved somewhere.

AWS S3 is good for file saving, and DocumentDB (MongoDB) and RedShift are good for web developments and BI reports.

Considering the portal for operators and drivers, and currently this project is a whole architect demonstration or investigation, DocumentDB (MongoDB) is selected for its simplicity and good compatibility, also cheaper.

However, the following shortcomings have to be confronted:

1. Poor performances for documents over 10 millions in single collection without well tweaked (indexes and shards etc are for production envrionment). Especially doing aggregation from raw data without OLAP features for daily and monthly trends analysis. This currently affects the web portal experiences.

2. Tools connecting PySpark (Python through JVM) and MongoDB are easy to use but poor compatibility supported between Python, Java and DB connecting. 
    
    1) PyMongo for Python: Easy for Python, but single thread to inserting and reading from MongoDB.
    2) MongoDB Connector for Spark (https://docs.mongodb.com/spark-connector): Tricky to use in Notebook and other Python IDE (it's written and used for JVM underneath), and very slow for reading or saving with big amount of data because of the language level data type conversion. 

Finally, **MongoDB Connector for Spark** is just for processed data exporting as it supports multiple threading (Thanks Java). **PyMongo** for web to read only (single thread but depending on the MongoDB performance). And **Pandas to_csv()** is used to export to AWS S3 or local for development usage purpose.

## 3. Web Portal Development

### The development stacks:
- Database: MongoDB (on AWS DocumentDB)
- Backend: Python Flask (on AWS EC2 or on Lambda)
- Frontend: VUE (on AWS S3)

Here, MongoDB is good for the data not strongly relational and documents/rows based; Flask can be easily work along with Python machine learning packages and a bunch of good scientific packages for real-time prediction tasks, and planned deoployed onto AWS Lambda; Vue on S3 is simple for demo development and is easy to deploy.

### Deployment

The backend api url has been configured with the specific AWS EC2 public address; And the CORS is configured in the backend flask app codes.

1. Frontend

   ```
   npm i
   npm run build
   ```

   Upload the content in dist onto the specific S3 buckets, which should be public access. The resource url of index.html is the web address for this portal. 

2. Backend

   Start the service from EC2 instance, and make sure the corresponding security groups have added appropriate inbound rule.

   From the directory containing 'flaskr' to run the commands:

   ```
   export FLASK_APP=flaskr
   export FLASK_ENV=development

   flask run --host 0.0.0.0 --port 20327
   ```
   For security consideration, the options for 'flask run' are essential, otherwise, the default setting is only for localhost access and with default port 5000, which is insecure.

### Portal design:
URL:
http://vue-taxi-2020.s3-us-west-2.amazonaws.com/index.html#/login

#### Notes: 
- Dud to the backend API is http, so the url from S3 has to be http as well.
- As no pre-calcution for the following statistics, these charts loading is about 2 mintues. And all the data is from **yellow_tripdata_2019_01**, it's about 10 millions of rows for that month.

#### Introduction:
1. Login: with whatever user name and passowrd so far.
2. Role selection
3. For Drivers: 
    
    Chart is: **Top 10 Areas of Taxi Demand in 10 Min Drive Distance**
        
    > Based on the current month, list all counts for each LocationID for trip <= 10 minuts. The feature for predicting not finished yet. Currently only statistics about 10 minutes drive is presented.
        
4. For Operators:

    Chart 1: **Top 10 Areas of Taxi Demand in 1 Hour**
    
    > Based on the current time (day and hour), list to top 10 LocationID. So the chart will change from hour to hour.
    
    Chart 2: **Trend of demand in 1 Month**
    
    > Based on the current month, show total pickups of each day.
    
    Chart 3: **Trend of Demand today**
    
    > Based on the selected LocationID and date, show total pickups of that location on every hour of that day. If nothing input, default LocationID is 230 (Times Sq/Theatre District, Manhattan), and date is the current date.
    
    Chart 4: **WeekDay's Demand in 1 Month**

    > Based on the selected LocationID, show total pickups of each weekday of current month. If nothing input, default LocationID is 230 (Times Sq/Theatre District, Manhattan).

# Further Thinking

1. For prediction, **Randomforestregressor** is used but not finished yet. For the prediction of Machine Learning, more features engineering work need to be done and most of the data have to be re-processed. Also extra data will be good and enhance the model training, such as, the data of weather and holidays in the past several years.
2. MongoDB itself is difficult to meet the requirements of multi-dimension analysis. OLAP cube based database services (RedShift, Apache Kylin) or well designed profiling (here for LocationID and crucial time or period) are key features.
3. For real-time simulation on map plotting, stream processing framework will bring much better user experience. Spark Streaming vs Flink are all good for EMR. 
4. On the web partal the LocationID mapping table was found on the last day. This could be done for the charts to be easily read. As this will re-process all database and need to be fully tested, this work could be done later.

# Notes
1. AWS EMR usage

    For current stage of feasibility evaluation, too much time was spent on the solution investigation and testing. Especially more and more issues about performance and local machine resources, EMR did help to enhance all of these, it is almost 5~10 times faster than the development environment on AWS EC2 single t2.medium instance, which is 2~5 times faster than my MacBook.
    
    However, when multiple csv files (data for more months) are processed, more and more AWS big data computing and database needed to  make this project more like production level. This is obviously not the target of the 14 days project. Also for the purpose to save time and money, most of the data used for this project are from yellow_tripdata_2009-01.csv, 02 and 03 also used to evaluation the DocumentDB query performance. Only these data are now stored on AWS DocumentDB. 
    
    Whereas, EMR just used for short time and simple tasks, i.e. ETL and additional features extended for csv files from Amazon S3 only. Most of the time the Jupyter notebooks are used on local laptop and the EC2 instance.

1. Shapefiles for geo coordinates

   P.S. From the NYC TLC website, Starting in July 2016, 
   Taxi_zones table contains the TLC's official taxi zone boundaries. the TLC no longer provides pickup and dropoff coordinates. Instead, each trip comes with taxi zone pickup and dropoff location IDs. The taxi zone shapefiles correspond to the pickup and drop-off zones, or LocationIDs, which are based on NYC Department of City Planning’s Neighborhood Tabulation Areas (NTAs) and are meant to approximate neighborhoods. 
   
   So far, good packages are not found yet to convert the NY zone boundaries data to traditional longitude/latitude, for which there are plenty of good tools/packages/plugins to visualize the map info. Therefore, pure Python Matplotlib is used straitforward to plot from scratch. This means the map plotting will be hugely enhanced when more tools are available.










