#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 10:33:54 2020

@author: zhengxin
"""


# %% Section 1: ETL for ML
fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"


pickup_time_col_name = 'tpep_pickup_datetime'
drop_time_col_name = 'tpep_dropoff_datetime'

csv_path = filepath + '/' + fn + '.csv'
pkl_path = csv_path + '_after' + '.pkl'


import pandas as pd
import numpy as np




#%% pickle loading as df
df = pd.read_pickle(pkl_path)


#%% feature engineering

cols_feature = [

# # 'VendorID', 
# # 'tpep_pickup_datetime', 
# # 'tpep_dropoff_datetime',
# 'passenger_count', 
# 'trip_distance', 
# 'RatecodeID', 
# # 'store_and_fwd_flag',
# 'PULocationID', 
# 'DOLocationID', 
# ## 'payment_type', 
# 'fare_amount', 
# ## 'extra',
# ## 'mta_tax', 
# ## 'tip_amount', 
# ## 'tolls_amount', 
# ## 'improvement_surcharge',
# 'total_amount', 
# ## 'congestion_surcharge', 
# 'pickup_month', 
# 'pickup_day',
# 'pickup_hour', 
# 'pickup_minutes', 
# 'pickup_weekday', 
# 'trip_duration',
# 'trip_speed'


    
## 'VendorID', 
# 'tpep_pickup_datetime', 
# 'tpep_dropoff_datetime',
'passenger_count', 
'trip_distance', 
# 'RatecodeID', 
## 'store_and_fwd_flag',
'PULocationID', 
'DOLocationID', 
## 'payment_type', 
# 'fare_amount', 
## 'extra',
## 'mta_tax', 
## 'tip_amount', 
## 'tolls_amount', 
## 'improvement_surcharge',
# 'total_amount', 
## 'congestion_surcharge', 
'pickup_month', 
'pickup_day',
'pickup_hour', 
'pickup_minutes', 
'pickup_weekday', 
# 'trip_duration',
# 'trip_speed'



]

    

## drop unnecessary cols
for col in df.columns:
    if col not in cols_feature:
        df.drop([col], axis=1, inplace=True)
        


# tpep_pickup_datetime       0
# passenger_count            0
# trip_distance              0
# RatecodeID                 0
# PULocationID               0
# DOLocationID               0
# fare_amount                0
# total_amount               0
# pickup_month               0
# pickup_day                 0
# pickup_hour                0
# pickup_minutes             0
# pickup_weekday             0
# trip_duration              0
# trip_speed              5968


#%% ETL with Pandas

# ##% dropna() find the loc of row with na
# df_isna = df.isna()
# df_isna.sum()
# index_isna= df_isna.loc[df_isna['trip_speed']==True].index
# df.drop(index_isna, inplace=True)

##% drop trip_distance == 0
df1 = df[(df['trip_distance']>0.1) & (df['trip_distance']<264)]

##% round trip_distance with digits=1
ds = df1.loc[:, 'trip_distance'].map(lambda x: np.round(x, 1))
df2 = df1.assign(trip_distance = ds.values)



#%% SAVE pkl after ETL for Pandas(FAILED for ML); csv for Spark
# df2.to_pickle(pkl_path + '_etl.pkl')
df2.to_csv(csv_path + '_etl.csv')


## LOAD ETL pkl
# import pands as pd
# df = pd.read_pickle(pkl_path + '_etl.pkl')



## %% test for pandas aggregation

# ## categorial columns can not do agg
# # df.dtypes 



# # df.groupby(['PULocationID', 'trip_distance'])['total_amount'].mean()

# agdf = df.groupby(['PULocationID', 'pickup_weekday', 'pickup_minutes']).aggregate(
#     {
#       "PULocationID": ['count'],
#       "total_amount":['mean'], 
#       "trip_distance":['mean']
#       })








#%% Section 2: AGGREGATION for Pickup Counting

fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"

csv_path = filepath + '/' + fn + '.csv'
pkl_path = csv_path + '_after' + '.pkl'


#%% Using Pandas aggregration: FAILED

# import pandas as pd
# import numpy as np


# pkl_etl_path = pkl_path + '_etl.pkl'
# df_pandas = pd.read_pickle(pkl_etl_path)


# ## agg for every column 
# ## this 5 columns groupby will take 10+GB swap by using Pandas, VERY SLOW, CANNOT BE DONE.
# agdf = df.groupby(['PULocationID', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday']).aggregate(
#     {
#       "PULocationID": ['count']
#     }
#     )



#%% Using Spark RDD to do Map and ReduceByKey (SUCESS)

fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"

csv_path = filepath + '/' + fn + '.csv'
csv_etl_path = csv_path + '_etl.csv'


from pyspark.sql import SparkSession
from pyspark import SparkContext

from pyspark.sql import functions as F

# from pyspark.sql import Row

# spark = SparkSession \
#     .builder \
#     .appName("Read CSVs") \
#     .getOrCreate()


sc = SparkContext('local','example')
rdd = sc.textFile(csv_etl_path)


# rdd.take(5)

# [',passenger_count,trip_distance,PULocationID,DOLocationID,pickup_month,pickup_day,pickup_hour,pickup_minutes,pickup_weekday',
#  '0,1,1.5,151,239,1,1,0,40,1',
#  '1,1,2.6,239,246,1,1,0,50,1',
#  '7,1,1.3,163,229,1,1,0,20,1',
#  '8,1,3.7,229,7,1,1,0,30,1']


## after ETL
# [
## 'passenger_count', 
# 'trip_distance', 
# 'PULocationID', 
# 'DOLocationID', 
## 'pickup_month', 
# 'pickup_day',
# 'pickup_hour', 
# 'pickup_minutes', 
# 'pickup_weekday'
# ]



header = rdd.first()
data = rdd.filter(lambda line: line != header)


rdd1 = data.map(lambda line: tuple(line.split(',')))
rdd2 = rdd1.map(lambda row: (*row[2:5], *row[6:]))
rdd3 = rdd2.map(lambda row: (row,1))
agrdd = rdd3.reduceByKey(lambda a,b: a + b)




## Save to textfile
csv_rddagg_path = csv_etl_path + '_rddagg.csv'
agrdd.repartition(1).saveAsTextFile(csv_rddagg_path)
# agrdd.saveAsTextFile(csv_rddagg_path)




#%% Using Spark Dataframe (SUCESS)

fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"

csv_path = filepath + '/' + fn + '.csv'
csv_etl_path = csv_path + '_etl.csv'


from pyspark.sql import SparkSession
from pyspark.sql import functions as F

# from pyspark.sql import Row

spark = SparkSession \
    .builder \
    .appName("Read CSVs") \
    .getOrCreate()
    
## Convert Pandas dataframe to Spark one TOO SLOW (single core)
df = spark.read.format('csv').options(header='true', inferSchema='true').load(csv_etl_path)


##%% agg for every column

    # .groupby('PULocationID', 'DOLocationID', 'trip_distance', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday')\
    # .groupby('PULocationID', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday')\
agdf = df\
    .groupby('PULocationID', 'DOLocationID', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday')\
    .agg(F.count('pickup_minutes').alias('COUNT'))
    # .orderBy('pickup_minutes', ascending=True)


csv_agg_path = csv_etl_path + '_dfagg.csv'

## OOM: because Pandas for large data
# pdf = agdf.toPandas()
# pdf.to_csv(csv_agg_path)

# agdf.write.csv(csv_agg_path)
agdf.repartition(1).write.csv(csv_agg_path)









#%% Section 3:  ML - Random Forest

fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"

csv_path = filepath + '/' + fn + '.csv'
csv_etl_path = csv_path + '_etl.csv'

csv_agg_path = csv_etl_path + '_dfagg.csv'


import numpy as np

from sklearn.metrics import mean_squared_error


import pyspark.sql.functions as F
from pyspark.sql import SQLContext, types
from pyspark.sql import SparkSession


spark = SparkSession \
    .builder \
    .appName("Read CSVs") \
    .getOrCreate()
    
    

# cols = ['PULocationID', 'DOLocationID', 'trip_distance', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday','COUNTS']
schema = types.StructType([
    types.StructField('PULocationID', types.IntegerType()),
    types.StructField('DOLocationID', types.IntegerType()),
    # types.StructField('trip_distance', types.DoubleType()),
    types.StructField('pickup_day', types.IntegerType()),
    types.StructField('pickup_hour', types.IntegerType()),
    types.StructField('pickup_minutes', types.IntegerType()),
    types.StructField('pickup_weekday', types.IntegerType()),
    types.StructField('COUNTS', types.IntegerType())
])

# rfdf = spark.read.format('csv').load(csv_agg_path).toDF(*cols)
rfdf = spark.read.csv(csv_agg_path, header=False, schema=schema)






#%%


#%% 2. Train / Test Split
# from sklearn.model_selection import train_test_split
# X = data.drop('COUNTS', axis=1)  
# y = data['COUNTS']
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.50, random_state = 2020, stratify=y)

dftrain, dfvalid, dftest = rfdf.randomSplit([0.6, 0.2, 0.2])

pdf_train = dftrain.toPandas()
pdf_valid = dfvalid.toPandas()



Xtrain = pdf_train.drop('COUNTS', axis=1)
ytrain = pdf_train['COUNTS']
Xvalid = pdf_valid.drop('COUNTS', axis=1)
yvalid = pdf_valid['COUNTS']





## Spark Style:
# ytrain = dftrain.select('COUNTS').rdd.flatMap(lambda x: x).collect()
# yvalid = dfvalid.select('COUNTS').rdd.flatMap(lambda x: x).collect()




#%% StandardScaler
from sklearn.preprocessing import StandardScaler
ss = StandardScaler()

X_train = Xtrain
X_valid = Xvalid

X_train_scaled = ss.fit_transform(X_train)
X_valid_scaled = ss.transform(X_valid)

## target distribution 
y_train = np.array(ytrain)
y_valid = np.array(yvalid)

# ## log10 them
# y_train = np.array(np.log10(ytrain))
# y_valid = np.array(np.log10(yvalid))

#%% RF single fit
from sklearn.ensemble import RandomForestRegressor
reg = RandomForestRegressor(n_estimators=1, max_depth=30, n_jobs=-1, warm_start=True)


reg.fit(X_train_scaled, y_train)
training_accuracy = reg.score(X_train_scaled, y_train)
valid_accuracy = reg.score(X_valid_scaled, y_valid)
rmsetrain = np.sqrt(mean_squared_error(reg.predict(X_train_scaled), y_train))
rmsevalid = np.sqrt(mean_squared_error(reg.predict(X_valid_scaled), y_valid))


#%% RF multiple fits
reg = RandomForestRegressor(n_estimators=1, max_depth=30, n_jobs=-1, warm_start=True)
for n in range(10,20):
    reg.set_params(n_estimators=n)
    reg.fit(X_train_scaled, y_train)
    training_accuracy = reg.score(X_train_scaled, y_train)
    valid_accuracy = reg.score(X_valid_scaled, y_valid)
    rmsetrain = np.sqrt(mean_squared_error(reg.predict(X_train_scaled), y_train))
    rmsevalid = np.sqrt(mean_squared_error(reg.predict(X_valid_scaled), y_valid))
    print("N = %d, R^2 (train) = %0.3f, R^2 (valid) = %0.3f, RMSE (train) = %0.3f, RMSE (valid) = %0.3f" % (n, training_accuracy, valid_accuracy, rmsetrain, rmsevalid))


## .groupby('PULocationID', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday')\
##
# N = 1, R^2 (train) = 0.967, R^2 (valid) = 0.911, RMSE (train) = 5.089, RMSE (valid) = 8.440
# N = 2, R^2 (train) = 0.981, R^2 (valid) = 0.930, RMSE (train) = 3.934, RMSE (valid) = 7.461
# N = 3, R^2 (train) = 0.985, R^2 (valid) = 0.938, RMSE (train) = 3.496, RMSE (valid) = 7.058
# N = 4, R^2 (train) = 0.987, R^2 (valid) = 0.941, RMSE (train) = 3.230, RMSE (valid) = 6.873
# N = 5, R^2 (train) = 0.988, R^2 (valid) = 0.943, RMSE (train) = 3.074, RMSE (valid) = 6.752
# N = 6, R^2 (train) = 0.989, R^2 (valid) = 0.945, RMSE (train) = 2.950, RMSE (valid) = 6.660
# N = 7, R^2 (train) = 0.990, R^2 (valid) = 0.946, RMSE (train) = 2.860, RMSE (valid) = 6.590
# N = 8, R^2 (train) = 0.990, R^2 (valid) = 0.946, RMSE (train) = 2.788, RMSE (valid) = 6.547
# N = 9, R^2 (train) = 0.991, R^2 (valid) = 0.947, RMSE (train) = 2.734, RMSE (valid) = 6.496
# N = 10, R^2 (train) = 0.991, R^2 (valid) = 0.948, RMSE (train) = 2.698, RMSE (valid) = 6.473
# N = 11, R^2 (train) = 0.991, R^2 (valid) = 0.948, RMSE (train) = 2.661, RMSE (valid) = 6.451
# N = 12, R^2 (train) = 0.991, R^2 (valid) = 0.948, RMSE (train) = 2.629, RMSE (valid) = 6.430
# N = 13, R^2 (train) = 0.991, R^2 (valid) = 0.949, RMSE (train) = 2.605, RMSE (valid) = 6.415
# N = 14, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.584, RMSE (valid) = 6.403
# N = 15, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.567, RMSE (valid) = 6.393
# N = 16, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.548, RMSE (valid) = 6.379
# N = 17, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.531, RMSE (valid) = 6.380
# N = 18, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.521, RMSE (valid) = 6.368
# N = 19, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.510, RMSE (valid) = 6.364
# N = 20, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.499, RMSE (valid) = 6.361
# N = 21, R^2 (train) = 0.992, R^2 (valid) = 0.949, RMSE (train) = 2.491, RMSE (valid) = 6.358
# N = 22, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.483, RMSE (valid) = 6.351
# N = 23, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.478, RMSE (valid) = 6.351
# N = 24, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.471, RMSE (valid) = 6.350
# N = 25, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.465, RMSE (valid) = 6.347
# N = 26, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.458, RMSE (valid) = 6.342
# N = 27, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.451, RMSE (valid) = 6.335
# N = 28, R^2 (train) = 0.992, R^2 (valid) = 0.950, RMSE (train) = 2.445, RMSE (valid) = 6.330
# N = 29, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.439, RMSE (valid) = 6.324
# N = 30, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.435, RMSE (valid) = 6.318
# N = 31, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.430, RMSE (valid) = 6.315
# N = 32, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.425, RMSE (valid) = 6.315
# N = 33, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.421, RMSE (valid) = 6.312
# N = 34, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.416, RMSE (valid) = 6.309
# N = 35, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.411, RMSE (valid) = 6.304
# N = 36, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.407, RMSE (valid) = 6.303
# N = 37, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.403, RMSE (valid) = 6.301
# N = 38, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.400, RMSE (valid) = 6.300
# N = 39, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.397, RMSE (valid) = 6.298
# N = 40, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.394, RMSE (valid) = 6.297
# N = 41, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.392, RMSE (valid) = 6.294
# N = 42, R^2 (train) = 0.993, R^2 (valid) = 0.950, RMSE (train) = 2.391, RMSE (valid) = 6.291
# N = 43, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.389, RMSE (valid) = 6.289
# N = 44, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.386, RMSE (valid) = 6.288
# N = 45, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.384, RMSE (valid) = 6.287
# N = 46, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.381, RMSE (valid) = 6.286
# N = 47, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.379, RMSE (valid) = 6.282
# N = 48, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.376, RMSE (valid) = 6.279
# N = 49, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.374, RMSE (valid) = 6.279
# N = 50, R^2 (train) = 0.993, R^2 (valid) = 0.951, RMSE (train) = 2.372, RMSE (valid) = 6.278



## .groupby('PULocationID', 'DOLocationID', 'pickup_day', 'pickup_hour', 'pickup_minutes', 'pickup_weekday')\
## ++ 'DOLocationID'
# N = 1, R^2 (train) = 0.800, R^2 (valid) = 0.499, RMSE (train) = 0.933, RMSE (valid) = 1.472
# N = 2, R^2 (train) = 0.872, R^2 (valid) = 0.604, RMSE (train) = 0.746, RMSE (valid) = 1.308
# N = 3, R^2 (train) = 0.896, R^2 (valid) = 0.639, RMSE (train) = 0.672, RMSE (valid) = 1.249
# N = 4, R^2 (train) = 0.909, R^2 (valid) = 0.658, RMSE (train) = 0.631, RMSE (valid) = 1.216
# N = 5, R^2 (train) = 0.916, R^2 (valid) = 0.669, RMSE (train) = 0.606, RMSE (valid) = 1.196
# N = 6, R^2 (train) = 0.920, R^2 (valid) = 0.676, RMSE (train) = 0.588, RMSE (valid) = 1.184
# N = 7, R^2 (train) = 0.924, R^2 (valid) = 0.681, RMSE (train) = 0.577, RMSE (valid) = 1.175
# N = 8, R^2 (train) = 0.926, R^2 (valid) = 0.685, RMSE (train) = 0.567, RMSE (valid) = 1.167
# N = 9, R^2 (train) = 0.928, R^2 (valid) = 0.687, RMSE (train) = 0.560, RMSE (valid) = 1.162
# N = 10, R^2 (train) = 0.930, R^2 (valid) = 0.690, RMSE (train) = 0.554, RMSE (valid) = 1.158
# N = 11, R^2 (train) = 0.931, R^2 (valid) = 0.692, RMSE (train) = 0.549, RMSE (valid) = 1.155
# N = 12, R^2 (train) = 0.932, R^2 (valid) = 0.693, RMSE (train) = 0.545, RMSE (valid) = 1.151
# N = 13, R^2 (train) = 0.933, R^2 (valid) = 0.695, RMSE (train) = 0.541, RMSE (valid) = 1.148
# N = 14, R^2 (train) = 0.933, R^2 (valid) = 0.696, RMSE (train) = 0.539, RMSE (valid) = 1.146
# N = 15, R^2 (train) = 0.934, R^2 (valid) = 0.697, RMSE (train) = 0.536, RMSE (valid) = 1.144
# N = 16, R^2 (train) = 0.934, R^2 (valid) = 0.698, RMSE (train) = 0.534, RMSE (valid) = 1.142
# N = 17, R^2 (train) = 0.935, R^2 (valid) = 0.699, RMSE (train) = 0.532, RMSE (valid) = 1.141
# N = 18, R^2 (train) = 0.935, R^2 (valid) = 0.699, RMSE (train) = 0.530, RMSE (valid) = 1.140
# N = 19, R^2 (train) = 0.936, R^2 (valid) = 0.700, RMSE (train) = 0.528, RMSE (valid) = 1.139
# N = 20, R^2 (train) = 0.936, R^2 (valid) = 0.701, RMSE (train) = 0.527, RMSE (valid) = 1.137
# N = 21, R^2 (train) = 0.937, R^2 (valid) = 0.701, RMSE (train) = 0.526, RMSE (valid) = 1.136
# N = 22, R^2 (train) = 0.937, R^2 (valid) = 0.702, RMSE (train) = 0.524, RMSE (valid) = 1.135
# N = 23, R^2 (train) = 0.937, R^2 (valid) = 0.702, RMSE (train) = 0.523, RMSE (valid) = 1.135
# N = 24, R^2 (train) = 0.937, R^2 (valid) = 0.703, RMSE (train) = 0.522, RMSE (valid) = 1.134
# N = 25, R^2 (train) = 0.938, R^2 (valid) = 0.703, RMSE (train) = 0.521, RMSE (valid) = 1.133
# N = 26, R^2 (train) = 0.938, R^2 (valid) = 0.703, RMSE (train) = 0.520, RMSE (valid) = 1.133
# N = 27, R^2 (train) = 0.938, R^2 (valid) = 0.703, RMSE (train) = 0.520, RMSE (valid) = 1.132
# N = 28, R^2 (train) = 0.938, R^2 (valid) = 0.704, RMSE (train) = 0.519, RMSE (valid) = 1.131
# N = 29, R^2 (train) = 0.938, R^2 (valid) = 0.704, RMSE (train) = 0.518, RMSE (valid) = 1.131
# N = 30, R^2 (train) = 0.938, R^2 (valid) = 0.704, RMSE (train) = 0.518, RMSE (valid) = 1.131
# N = 31, R^2 (train) = 0.939, R^2 (valid) = 0.704, RMSE (train) = 0.517, RMSE (valid) = 1.130
# N = 32, R^2 (train) = 0.939, R^2 (valid) = 0.705, RMSE (train) = 0.516, RMSE (valid) = 1.130
# N = 33, R^2 (train) = 0.939, R^2 (valid) = 0.705, RMSE (train) = 0.516, RMSE (valid) = 1.130
# N = 34, R^2 (train) = 0.939, R^2 (valid) = 0.705, RMSE (train) = 0.515, RMSE (valid) = 1.129
# N = 35, R^2 (train) = 0.939, R^2 (valid) = 0.705, RMSE (train) = 0.515, RMSE (valid) = 1.129
# N = 36, R^2 (train) = 0.939, R^2 (valid) = 0.705, RMSE (train) = 0.514, RMSE (valid) = 1.128
# N = 37, R^2 (train) = 0.939, R^2 (valid) = 0.705, RMSE (train) = 0.514, RMSE (valid) = 1.128
# N = 38, R^2 (train) = 0.939, R^2 (valid) = 0.706, RMSE (train) = 0.513, RMSE (valid) = 1.128
# N = 39, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.513, RMSE (valid) = 1.127
# N = 40, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.513, RMSE (valid) = 1.127
# N = 41, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.512, RMSE (valid) = 1.127
# N = 42, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.512, RMSE (valid) = 1.127
# N = 43, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.512, RMSE (valid) = 1.127
# N = 44, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.511, RMSE (valid) = 1.126
# N = 45, R^2 (train) = 0.940, R^2 (valid) = 0.706, RMSE (train) = 0.511, RMSE (valid) = 1.126
# N = 46, R^2 (train) = 0.940, R^2 (valid) = 0.707, RMSE (train) = 0.511, RMSE (valid) = 1.126
# N = 47, R^2 (train) = 0.940, R^2 (valid) = 0.707, RMSE (train) = 0.510, RMSE (valid) = 1.126
# N = 48, R^2 (train) = 0.940, R^2 (valid) = 0.707, RMSE (train) = 0.510, RMSE (valid) = 1.126
# N = 49, R^2 (train) = 0.940, R^2 (valid) = 0.707, RMSE (train) = 0.509, RMSE (valid) = 1.125
# N = 50, R^2 (train) = 0.940, R^2 (valid) = 0.707, RMSE (train) = 0.509, RMSE (valid) = 1.125



#%% IMPORTANCE: plot ONE reg.feature_importances_

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from IPython.display import display

## DISPLAY target coluns distribution
sns.set()
sns.relplot(data=ytrain.value_counts())
sns.relplot(data=np.log10(ytrain.value_counts()))
display(ytrain.describe())

## DISPLAY feature importance
feats = {}
for feature, importance in zip(rfdf.columns, reg.feature_importances_):
    feats[feature] = importance
    
importances = pd.DataFrame.from_dict(feats, orient='index').rename(columns={0: 'Gini-Importance'})
importances = importances.sort_values(by='Gini-Importance', ascending=False)
importances = importances.reset_index()
importances = importances.rename(columns={'index': 'Features'})
sns.set(font_scale = 5)
sns.set(style="whitegrid", color_codes=True, font_scale = 1.7)
fig, ax = plt.subplots()
fig.set_size_inches(30,15)
sns.barplot(x=importances['Gini-Importance'], y=importances['Features'], data=importances, color='skyblue')
plt.xlabel('Importance', fontsize=25, weight = 'bold')
plt.ylabel('Features', fontsize=25, weight = 'bold')
plt.title('Feature Importance', fontsize=25, weight = 'bold')
display(plt.show())
display(importances)



# %% 5. PCA (Principal Component Analysis)

import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA
pca_test = PCA(n_components=6)
pca_test.fit(X_train_scaled)
sns.set(style='whitegrid')
plt.plot(np.cumsum(pca_test.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance')
plt.axvline(linewidth=4, color='r', linestyle = '--', x=6, ymin=0, ymax=1)
display(plt.show())

evr = pca_test.explained_variance_ratio_
cvr = np.cumsum(pca_test.explained_variance_ratio_)
pca_df = pd.DataFrame()
pca_df['Cumulative Variance Ratio'] = cvr
pca_df['Explained Variance Ratio'] = evr
display(pca_df.head(10))


## so change the n_components=10
pca = PCA(n_components=5)
pca.fit(X_train_scaled)
X_train_scaled_pca = pca.transform(X_train_scaled)
X_valid_scaled_pca = pca.transform(X_valid_scaled)

# %% PCA components w/ weights
pca_dims = []
for x in range(0, len(pca_df)):
    pca_dims.append('PCA Component {}'.format(x))
    
pca_test_df = pd.DataFrame(pca_test.components_, columns=Xtrain.columns, index=pca_dims)
display(pca_test_df.head(10).T)



#%% RF(with PCS components) multiple fits: NO IMPROVEMENT
X_train_scaled = X_train_scaled_pca
X_valid_scaled = X_valid_scaled_pca

reg = RandomForestRegressor(n_estimators=1, max_depth=30, n_jobs=-1, warm_start=True)
for n in range(10,20):
    reg.set_params(n_estimators=n)
    reg.fit(X_train_scaled, y_train)
    training_accuracy = reg.score(X_train_scaled, y_train)
    valid_accuracy = reg.score(X_valid_scaled, y_valid)
    rmsetrain = np.sqrt(mean_squared_error(reg.predict(X_train_scaled), y_train))
    rmsevalid = np.sqrt(mean_squared_error(reg.predict(X_valid_scaled), y_valid))
    print("N = %d, R^2 (train) = %0.3f, R^2 (valid) = %0.3f, RMSE (train) = %0.3f, RMSE (valid) = %0.3f" % (n, training_accuracy, valid_accuracy, rmsetrain, rmsevalid))

# N = 10, R^2 (train) = 0.862, R^2 (valid) = 0.532, RMSE (train) = 0.776, RMSE (valid) = 1.422
# N = 11, R^2 (train) = 0.865, R^2 (valid) = 0.535, RMSE (train) = 0.768, RMSE (valid) = 1.418
# N = 12, R^2 (train) = 0.867, R^2 (valid) = 0.538, RMSE (train) = 0.761, RMSE (valid) = 1.413
# N = 13, R^2 (train) = 0.869, R^2 (valid) = 0.541, RMSE (train) = 0.755, RMSE (valid) = 1.408
# N = 14, R^2 (train) = 0.869, R^2 (valid) = 0.545, RMSE (train) = 0.754, RMSE (valid) = 1.403
# N = 15, R^2 (train) = 0.871, R^2 (valid) = 0.547, RMSE (train) = 0.749, RMSE (valid) = 1.399
# N = 16, R^2 (train) = 0.872, R^2 (valid) = 0.549, RMSE (train) = 0.745, RMSE (valid) = 1.396
# N = 17, R^2 (train) = 0.874, R^2 (valid) = 0.551, RMSE (train) = 0.741, RMSE (valid) = 1.393
# N = 18, R^2 (train) = 0.875, R^2 (valid) = 0.553, RMSE (train) = 0.739, RMSE (valid) = 1.391
# N = 19, R^2 (train) = 0.876, R^2 (valid) = 0.554, RMSE (train) = 0.735, RMSE (valid) = 1.388



# %% Hyperparameter Tuning: RandomSearchCV 

reg_2 = RandomForestRegressor(n_estimators=1, max_depth=30, n_jobs=-1, warm_start=True)

from sklearn.model_selection import RandomizedSearchCV
n_estimators = [int(x) for x in np.linspace(start = 10, stop = 100, num = 10)]
max_features = ['log2', 'sqrt']
max_depth = [int(x) for x in np.linspace(start = 1, stop = 15, num = 15)]
min_samples_split = [int(x) for x in np.linspace(start = 2, stop = 50, num = 10)]
min_samples_leaf = [int(x) for x in np.linspace(start = 2, stop = 50, num = 10)]
bootstrap = [True, False]
param_dist = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap}
rs = RandomizedSearchCV(reg_2, 
                        param_dist, 
                        n_iter = 100, 
                        cv = 3, 
                        verbose = 1, 
                        n_jobs=-1, 
                        random_state=0)
rs.fit(X_train_scaled_pca, y_train)
display(rs.best_params_)

# # Fitting 3 folds for each of 100 candidates, totalling 300 fits
# # [Parallel(n_jobs=-1)]: Using backend LokyBackend with 4 concurrent workers.
# # [Parallel(n_jobs=-1)]: Done  42 tasks      | elapsed: 65.3min
# # [Parallel(n_jobs=-1)]: Done 192 tasks      | elapsed: 281.6min
# # [Parallel(n_jobs=-1)]: Done 300 out of 300 | elapsed: 429.1min finished
# {'n_estimators': 60,
#   'min_samples_split': 23,
#   'min_samples_leaf': 2,
#   'max_features': 'sqrt',
#   'max_depth': 15,
#   'bootstrap': False}
