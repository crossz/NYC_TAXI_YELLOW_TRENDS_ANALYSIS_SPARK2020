#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 10:33:54 2020

@author: zhengxin
"""


# %% init
fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"


pickup_time_col_name = 'tpep_pickup_datetime'
drop_time_col_name = 'tpep_dropoff_datetime'



import pandas as pd
import os
from datetime import datetime


csv_path = filepath + '/' + fn + '.csv'
pkl_path_before = csv_path + '.pkl'


#%% csv loading
df_csv = pd.read_csv(csv_path,header=0)


#%% feature engineering
'''
This part can be ignored or adjusted, depending on which stage to do the featuring engineering.
Note. No ETL in this script, because trying to keep all rows.
'''

col_pickup_datetime = df_csv[pickup_time_col_name].apply(lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
col_dropoff_datetime = df_csv[drop_time_col_name].apply(lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))


col_trip_duration = (col_dropoff_datetime - col_pickup_datetime).apply(lambda x: x.total_seconds()/3600)
col_trip_speed = df_csv['trip_distance']/col_trip_duration

## multiple columns(series) generated: TOO SLOW and TOO MUCH MEMORY/SWP
# cols_pickup_dtarray = col_pickup_datetime.apply(lambda x: pd.Series([x.year, x.month, x.day, x.hour, (x.hour*60 + x.minute)//10*10, x.weekday()]))
col_pickup_month = col_pickup_datetime.apply(lambda x: x.month)
col_pickup_day = col_pickup_datetime.apply(lambda x: x.day)
col_pickup_hour = col_pickup_datetime.apply(lambda x: x.hour)
col_pickup_minutes = col_pickup_datetime.apply(lambda x: (x.hour*60 + x.minute)//10*10)
col_pickup_weekday = col_pickup_datetime.apply(lambda x: x.weekday())

df_csv['pickup_month'] = col_pickup_month
df_csv['pickup_day'] = col_pickup_day
df_csv['pickup_hour'] = col_pickup_hour
df_csv['pickup_minutes'] = col_pickup_minutes
df_csv['pickup_weekday'] = col_pickup_weekday
df_csv['trip_duration'] = col_trip_duration
df_csv['trip_speed'] = col_trip_speed



# %% csv vs pickle: stats about raw data loading and saving
df_csv.shape
# (7667792, 18)

print(os.stat(csv_path).st_size)
# 687,088,084

df_csv.memory_usage(deep=True).sum() 
# 2,591,713,824

df_csv.dtypes
# VendorID                   int64
# tpep_pickup_datetime      object
# tpep_dropoff_datetime     object
# passenger_count            int64
# trip_distance            float64
# RatecodeID                 int64
# store_and_fwd_flag        object
# PULocationID               int64
# DOLocationID               int64
# payment_type               int64
# fare_amount              float64
# extra                    float64
# mta_tax                  float64
# tip_amount               float64
# tolls_amount             float64
# improvement_surcharge    float64
# total_amount             float64
# congestion_surcharge     float64
# dtype: object

df_csv.memory_usage(deep=True)
# Index                          128
# VendorID                  61342336
# tpep_pickup_datetime     582752192
# tpep_dropoff_datetime    582752192
# passenger_count           61342336
# trip_distance             61342336
# RatecodeID                61342336
# store_and_fwd_flag       506074272
# PULocationID              61342336
# DOLocationID              61342336
# payment_type              61342336
# fare_amount               61342336
# extra                     61342336
# mta_tax                   61342336
# tip_amount                61342336
# tolls_amount              61342336
# improvement_surcharge     61342336
# total_amount              61342336
# congestion_surcharge      61342336
# dtype: int64

##% pickle saving
df_csv.to_pickle(pkl_path_before)

## pickle: stats about loading
# print(os.stat(pkl_path_before).st_size)
## 1,129,355,286


# %timeit pd.read_csv(csv_path,header=0)
# 20.7 s ± 806 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
# %timeit pd.read_pickle(pkl_path_before)
# 4.32 s ± 248 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)


#%% pickle: stats for "astype('category')"

# df_csv.tpep_pickup_datetime.astype('category').head()
# df_csv.tpep_pickup_datetime.astype('category').memory_usage(deep=True)
# 285250336 vs 582752192

# df_csv.PULocationID.astype('category').head()
# df_csv.PULocationID.astype('category').memory_usage(deep=True)
# 15348056 vs 61342336

## No need to do aggreating on these columns
columns_to_agg = ['passenger_count', 'trip_distance', 'fare_amount', 'tip_amount', 'total_amount', 'trip_duration', 'trip_speed']

for col in df_csv.columns:
    if col not in columns_to_agg:
        df_csv[col] = df_csv[col].astype('category')
    
df_csv.memory_usage(deep=True).sum()
# 748289504 vs 2591713824


## pickle saving after 
pkl_path_after = csv_path + '_after' + '.pkl'
df_csv.to_pickle(pkl_path_after)

# print(os.stat(pkl_path_after).st_size)
# # 336866991

## loading time comparison
# %timeit pd.read_pickle(pkl_path_after)
# 1.4 s ± 53.9 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

# %timeit pd.read_csv(csv_path,header=0)
# 19.2 s ± 434 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

