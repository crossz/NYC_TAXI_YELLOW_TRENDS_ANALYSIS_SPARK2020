## pyspark --packages org.mongodb.spark:mongo-spark-connector_2.11:2.2.7

# %% init
fn = "yellow_tripdata_2019-01"
filepath = "../../../Downloads/csv"


pickup_time_col_name = 'tpep_pickup_datetime'
drop_time_col_name = 'tpep_dropoff_datetime'



from pyspark.sql import functions as F
from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType
from pyspark.sql.types import DoubleType

collname = fn.replace('-', '_')
spark = SparkSession \
    .builder \
    .appName("Read CSVs") \
    .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/nyctaxi." + collname) \
    .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/nyctaxi." + collname) \
    .getOrCreate()


# collname = fn[:fn.find('-')]
# spark = SparkSession \
#     .builder \
#     .appName("Read CSVs") \
#     .config("spark.mongodb.input.uri", "mongodb://cross2020:cross2020@docdb-2020-03-25-02-18-11.cluster-cl1egjscp0e8.us-west-2.docdb.amazonaws.com:27017/nyctaxi." + collname + "?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false") \
#     .config("spark.mongodb.output.uri", "mongodb://cross2020:cross2020@docdb-2020-03-25-02-18-11.cluster-cl1egjscp0e8.us-west-2.docdb.amazonaws.com:27017/nyctaxi." + collname + "?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false") \
#     .getOrCreate()



# %% load csv
df = spark.read.format('csv').options(header='true', inferSchema='true').load(filepath + '/' + fn + '.csv')


# %% Extra Columns appending   
weekDay =  F.udf(lambda x: x.weekday()) # 0: Monday
hourDay = F.udf(lambda x: x.hour)
minuteDay = F.udf(lambda x: (x.hour*60 + x.minute)//10*10)
dayDay = F.udf(lambda x: x.day)
monthYear = F.udf(lambda x: x.month)
yearYear = F.udf(lambda x: x.year)

minuteTrip = F.udf(lambda x, y: (y - x).total_seconds()/60)

df = df.withColumn('pickup_year', yearYear(F.col(pickup_time_col_name)).cast(IntegerType()))
df = df.withColumn('pickup_month', monthYear(F.col(pickup_time_col_name)).cast(IntegerType()))
df = df.withColumn('pickup_day', dayDay(F.col(pickup_time_col_name)).cast(IntegerType()))
df = df.withColumn('pickup_hour', hourDay(F.col(pickup_time_col_name)).cast(IntegerType()))
df = df.withColumn('pickup_weekday', weekDay(F.col(pickup_time_col_name)).cast(IntegerType()))
df = df.withColumn('pickup_minute', minuteDay(F.col(pickup_time_col_name)).cast(IntegerType()))
df = df.withColumn('travel_time', minuteTrip(F.col(pickup_time_col_name), F.col(drop_time_col_name)).cast(DoubleType()))
df = df.withColumn('travel_speed', (F.col('trip_distance')/minuteTrip(F.col(pickup_time_col_name), F.col(drop_time_col_name))*60).cast(DoubleType()))



# %% Data cleaning
clean_col = 'trip_distance'
df1 = df.filter((F.col(clean_col) > 0.1) & (F.col(clean_col) < 264))

clean_col = 'total_amount'
df2 = df1.filter((F.col(clean_col) > 0.1) & (F.col(clean_col) < 1000))

clean_col = 'passenger_count'
df3 = df2.filter((F.col(clean_col) >= 0) & (F.col(clean_col) < 7))


# %%
from datetime import datetime
startTime = datetime.now()

df3.write.format("mongo").mode("append").save()

dt = datetime.now() - startTime
print(dt)

