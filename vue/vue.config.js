const path = require('path');

function resolve(dir) {
	return path.join(__dirname, dir)
}
module.exports = {
	lintOnSave: true,
	chainWebpack: (config) => {
		config.resolve.alias
			.set('@', resolve('src'))
			.set('@views', resolve('src/views'))
			.set('@assets', resolve('src/assets'))
			.set('@components', resolve('src/components'))
			.set('@json', resolve('static/json'))
		config.plugins.delete('prefetch')
		config.module
			.rule('images')
			.use('url-loader')
			.loader('url-loader')
			.tap(options => Object.assign(options, { limit: 51200 }))
		config.output.filename('[name].[hash].js').end()
	},
	css: {
		loaderOptions: {
			sass: {
				// data: `@import "@assets/sass/modules/vars.scss";`
			}
		}
	},
	filenameHashing: false, // 文件名加hash 不包括js和img
	devServer: {
		host: 'localhost',
		port: 8080,
		// https: false,
		// hotOnly: false,
		disableHostCheck: true,
		// proxy: null,
		proxy: {   // 设置代理
		    '/': {
		        target: 'http://54.186.33.237:20327',
		        ws: true,
		        changeOrigin: true
		    },
		},
	}
}
