import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// 导入全局样式和字体文件
import './assets/sass/common.scss';
// 挂载全局request
import http from '@/utils/request';

// 引入路由拦截
router.beforeEach((to, from, next) => {
  // 判断没有用户信息 跳转登录页面
  let userData = sessionStorage.getItem('userData')
	if (userData) {
		next()
	} else if (to.name == 'Login') {
		next()
	} else {
    next('/login')
  }


})

Vue.prototype.$ajax = http

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
