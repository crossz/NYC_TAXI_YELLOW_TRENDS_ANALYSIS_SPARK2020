import axios from 'axios'
import qs from 'qs'
import Vue from 'vue'
import store from '../store/index.js'
import Routers from '../router/index.js'
/**
 * 创建axios实例
 * @type {[type]}
 */
const http = axios.create({
    baseURL: 'http://54.186.33.237:20327',
    withCredentials: false,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
    },
    timeout: 1000 * 60 * 10
    // transformRequest: [function (data) {
    //   let newData = '';
    //   for (let k in data) {
    //     if (data.hasOwnProperty(k) === true) {
    //       newData += encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) + '&';
    //     }
    //   }
    //   return newData;
    // }]
})

/**
 * 配置请求拦截
 */
http.interceptors.request.use(config => {

    // 以json方式传参的post方法
    if(config.data && config.data.json){
        config.headers['Content-Type'] = 'application/json'
        delete config.data['json']
    }else{   // 以form-data方式传参的post方法 不包括 multipart
        if(config.headers['Content-Type'] !== 'multipart/form-data'){
          config.data = qs.stringify({ ...config.data})
        }
    }


    return config
}, error => {    //请求错误处理

    Promise.reject(error)

})


/**
 * 配置响应拦截 404 403 500
 */
http.interceptors.response.use(response => {  //成功请求到数据
    // 请求完成关闭loading
    // Indicator.close();
    //这里根据后端提供的数据进行对应的处理
        switch (response.status) {
            case 200:
                return response.data
                break;
            case 404:
                // Routers.push('/notFound')
                console.log('请求接口资源不存在')
                break;
            case 500:
                // Routers.push('/error')
                console.log(response.message)
                break;
            default:
                // Routers.push('/error')
                break;
        }
    },
    error => { //响应错误处理

        console.log(JSON.stringify(error))
        // 获取错误码
        // let errCode = JSON.parse(JSON.stringify(error).response.status)
        // 网络请求失败 同意提示 也可以关闭
        // Toast.show(errCode);
        // Promise.reject(error.message)
    }
);


export default http;
