'''
export FLASK_APP=flaskr
export FLASK_ENV=development

flask run --host 0.0.0.0 --port 20327
'''

dbname = "nyctaxi"
collectionname = "yellow_tripdata_2019_01"


import os
from datetime import datetime
from bson.son import SON

import pandas as pd
from pandas import json_normalize
import json

from flask_cors import CORS
from flask_pymongo import PyMongo
from flask import jsonify
from flask import request
from flask import Flask


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    cors = CORS(app, resources={r"/*": {"origins": "*"}})
    app.config.from_mapping(
        MONGO_URI="mongodb://localhost:27017/" + dbname,
        # MONGO_URI="mongodb://cross2020:cross2020@docdb-2020-03-25-02-18-11.cluster-cl1egjscp0e8.us-west-2.docdb.amazonaws.com:27017/" + dbname + "?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false",
        SECRET_KEY='dev',
        # DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )
    # app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"
    mongo = PyMongo(app)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass


    ## Default: Test mongo connection
    @app.route("/")
    def home_page():
        mgdoc = mongo.db[collectionname].find(
            {
                "pickup_minute": 1430
            },
            {'_id': 0}
        ).limit(1)

        # return render_template("index.html",
        #     online_users=online_users)
            
        return jsonify([cur for cur in mgdoc])


    ## Demo: GET
    ## http://localhost:5000/hello?key1=aaa&key2=bbb
    @app.route('/hello')
    def hello():
        args = request.args
        print (args) # For debugging
        no1 = args['key1']
        no2 = args['key2']
        return jsonify(dict(data=[no1, no2])) 


    # ## Test: grouped by 3 cols: SLOW
    # @app.route("/aggregationtest")
    # def aggregationtest():

    #     mgdoc = mongo.db[collectionname].aggregate([
    #             {"$match": {"pickup_hour": {'$lte': hournow}, "pickup_weekday": weeekdaynow}},
    #             {
    #                 "$group":{
    #                 "_id": 
    #                         {
    #                         "PULocationID": "$PULocationID",
    #                         "pickup_minute": "$pickup_minute", 
    #                         "pickup_weekday": "$pickup_weekday"
    #                         },
                         
    #                 "group_SUM": {"$sum": 1},
    #                 "total_amount_AVG": {"$avg": "$total_amount"},
    #                 "travel_time_AVG": {"$avg": "$travel_time"},
    #                 "travel_speed_AVG": {"$avg": "$travel_speed"}
    #                 }
    #             },
    #             # {"$sort": SON([("group_SUM", -1)])},
    #             # {"$limit": 50}
    #         ])

    #     mgdoclist = []
    #     for cur in mgdoc:
    #         cur.update(cur.pop('_id'))
    #         mgdoclist.append(cur)

    #     return jsonify(dict(data=mgdoclist))


    # ## Section 1: For Operators
    # ## 1. 热门叫车地点的罗列 PULocationID/group_COUNT
    @app.route("/curhourbylocations10")
    def curhourbylocations10():
 
        weeekdaynow = datetime.today().weekday()
        daynow = datetime.now().day
        hournow = datetime.now().hour
        
        mgdoc = mongo.db[collectionname].aggregate([
                {"$match": {"pickup_hour": hournow, "pickup_day": daynow}},
                {
                    "$group":{
                    "_id": 
                            {
                            "PULocationID": "$PULocationID",
                            # "pickup_minute": "$pickup_minute", 
                            # "pickup_weekday": "$pickup_weekday"
                            },
                         
                    "group_COUNT": {"$sum": 1},
                    "total_amount_AVG": {"$avg": "$total_amount"},
                    "travel_time_AVG": {"$avg": "$travel_time"},
                    "travel_speed_AVG": {"$avg": "$travel_speed"}
                    }
                },
                {"$sort": SON([("group_COUNT", -1)])},
                {"$limit": 10}
            ])

        mgdoclist = []
        for cur in mgdoc:
            cur.update(cur.pop('_id'))
            mgdoclist.append(cur)

        return jsonify(dict(data=mgdoclist))



    ## 2. 本月平均每天叫车次数展示
    @app.route("/curmonthbyday")
    def curmonthpicksbyday():

        weeekdaynow = datetime.today().weekday()
        daynow = datetime.now().day
        hournow = datetime.now().hour

        mgdoc = mongo.db[collectionname].aggregate([
                # {"$match": {"pickup_month": 1}},
                {
                    "$group":{
                    "_id": 
                            {
                            "pickup_day": "$pickup_day",
                            # "pickup_minute": "$pickup_minute", 
                            # "pickup_weekday": "$pickup_weekday"
                            },
                         
                    "group_COUNT": {"$sum": 1},
                    "total_amount_AVG": {"$avg": "$total_amount"},
                    "travel_time_AVG": {"$avg": "$travel_time"},
                    "travel_speed_AVG": {"$avg": "$travel_speed"}
                    }
                },
                {"$sort": SON([("_id.pickup_day", 1)])},
                # {"$limit": 10}
            ])

        mgdoclist = []
        for cur in mgdoc:
            cur.update(cur.pop('_id'))
            mgdoclist.append(cur)

        return jsonify(dict(data=mgdoclist))


    ## 3. 指定叫车地点 PULocationID 的趋势展示 230, 当日, 每个小时的叫车次数和其他统计
    ## /curhourbylocandday?loc=230&day=1
    @app.route("/curhourbylocandday")
    def curhourbylocandday():

        weeekdaynow = datetime.today().weekday()
        daynow = datetime.now().day
        hournow = datetime.now().hour

        args = request.args
        print (args) # For debugging
        # theloc = int(args['loc'])
        # theday = int(args['day'])
        theloc = int(args['loc']) if args['loc'] == None else 230 ## default: 230
        theday = int(args['day']) if args['day'] == None else daynow

        mgdoc = mongo.db[collectionname].aggregate([
                {"$match": {"PULocationID": theloc, "pickup_day": theday}},
                {
                    "$group":{
                    "_id": 
                            {
                            "pickup_hour": "$pickup_hour",
                            # "pickup_minute": "$pickup_minute", 
                            # "pickup_weekday": "$pickup_weekday"
                            },
                         
                    "group_COUNT": {"$sum": 1},
                    "total_amount_AVG": {"$avg": "$total_amount"},
                    "travel_time_AVG": {"$avg": "$travel_time"},
                    "travel_speed_AVG": {"$avg": "$travel_speed"}
                    }
                },
                {"$sort": SON([("_id.pickup_hour", 1)])},
                # {"$limit": 10}
            ])

        mgdoclist = []
        for cur in mgdoc:
            cur.update(cur.pop('_id'))
            mgdoclist.append(cur)

        return jsonify(dict(data=mgdoclist))




        # # ## 4. 指定叫车地点 PULocationID 的趋势展示 230, 本月各 weekday 每天的叫车次数统计


    ## 4. 指定叫车地点 PULocationID 的趋势展示 230, 这一个月有几个周一到周日,然后求平均每个weekday,叫车次数: 要 mongo 2 次,太麻烦.
    ## /curweekdaybyloc?loc=230
    @app.route("/curweekdaybyloc")
    def curweekdaybyloc():

        weeekdaynow = datetime.today().weekday()
        daynow = datetime.now().day
        hournow = datetime.now().hour

        args = request.args
        print (args) # For debugging
        # theloc = int(args['loc'])
        theloc = int(args['loc']) if args['loc'] == None else 230 ## default: 230

        mgdoc = mongo.db[collectionname].aggregate([
                {"$match": {"PULocationID": theloc}},
                {
                    "$group":{
                    "_id": 
                            {
                            "pickup_weekday": "$pickup_weekday"
                            },
                         
                    "group_COUNT": {"$sum": 1},
                    "total_amount_AVG": {"$avg": "$total_amount"},
                    "travel_time_AVG": {"$avg": "$travel_time"},
                    "travel_speed_AVG": {"$avg": "$travel_speed"}
                    }
                },
                {"$sort": SON([("_id.pickup_weekday", 1)])},
                # {"$limit": 10}
            ])

        mgdoc1 = mongo.db[collectionname].aggregate([
                {"$match": {"PULocationID": 230}},
                {
                    "$group":{
                    "_id": 
                            {
                            "pickup_weekday": "$pickup_weekday",
                            "pickup_day": "$pickup_day"
                            },
                         
                    "group_COUNT": {"$sum": 1},
                    # "total_amount_AVG": {"$avg": "$total_amount"},
                    # "travel_time_AVG": {"$avg": "$travel_time"},
                    # "travel_speed_AVG": {"$avg": "$travel_speed"}
                    }
                },
                {"$sort": SON([("_id.pickup_day", 1)])},
                # {"$limit": 10}
            ])

        ## how many specific weekday in a month
        weekdaycount = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
        for doc1 in mgdoc1:
            if doc1['group_COUNT'] > 100:
                weekdaycount.update({doc1['_id']['pickup_weekday']:weekdaycount[doc1['_id']['pickup_weekday']] + 1}) 
        print(weekdaycount)

        mgdoclist = []
        for cur in mgdoc:
            cur.update(cur.pop('_id'))
            cur['group_COUNT'] = cur['group_COUNT']/weekdaycount[cur['pickup_weekday']]
            mgdoclist.append(cur)

        return jsonify(dict(data=mgdoclist))


    ## Seciont 2: For drivers
    ## 车程 10 分钟以内的:热门叫车地点的罗列 PULocationID/group_COUNT
    @app.route('/demand10')
    def demand10():
        mgdoc = mongo.db[collectionname].aggregate([
                # {"$match": {"pickup_hour": hournow, "pickup_day": daynow, "travel_time": {"$lte": 10}}},
                {"$match": {"travel_time": {"$lte": 10}}},
                {
                    "$group":{
                    "_id": 
                            {
                            "PULocationID": "$PULocationID",
                            # "pickup_minute": "$pickup_minute", 
                            # "pickup_weekday": "$pickup_weekday"
                            },
                         
                    "group_COUNT": {"$sum": 1},
                    "total_amount_AVG": {"$avg": "$total_amount"},
                    "travel_time_AVG": {"$avg": "$travel_time"},
                    "travel_speed_AVG": {"$avg": "$travel_speed"}
                    }
                },
                {"$sort": SON([("group_COUNT", -1)])},
                {"$limit": 10}
            ])

        mgdoclist = []
        for cur in mgdoc:
            cur.update(cur.pop('_id'))
            mgdoclist.append(cur)

        return jsonify(dict(data=mgdoclist))

        ## convert to pandas dataframe 
        # bb = json_normalize(mgdoclist)
        # print(bb.count())
        ## ->
        # returnedjson = jsonify(dict(data=mgdoclist))
        # return returnedjson

    return app